# High Stakes App
- version :: 1.0.0

# How to
- Server Host: http://ec2-34-193-105-44.compute-1.amazonaws.com
- App Repository: https://bitbucket.org/\_tiagoaguiar\_/hsacademyapp
- Server Repository: https://bitbucket.org/\_tiagoaguiar\_/highstakes-ws
- Video to show a little bit about how the app works: https://drive.google.com/file/d/0B79vvZnRjg1FelNhblktdVpDb3M/view?usp=sharing

# Notes
The app use token and session to login and keep it authenticate.
All server has been developed in Java with Google Guice, Google Sitebricks
and my own libraries that I developed recently.

The Android App was made from scratch and the architecture use concepts of
Design pattern Builder, Loader, [Fluent Interface](http://martinfowler.com/bliki/FluentInterface.html),
Async Tasks and more.

## Cool Features (implemented)
- Design is not just what it looks like and feels like. Design is how it works. ~ Steve Jobs
- Splash Screen with animation (increase experience and branding)
- Keep user connected by your token. He does not need to login every time.
- Dashboard and profile (Features High Stakes.. Snaps, posts, videos, podcasts, etc)

## Cool Features (not implemented)
- Develop iOS Version of this App
- Login with Facebook
- UnitTests
- Access through SSL connection
- Forgot Password (send email to keep safe)
- Internationalization of text and Youtube Video.
- terms of services and privacy policy in footer.

Thanks for reading,
Tiago
