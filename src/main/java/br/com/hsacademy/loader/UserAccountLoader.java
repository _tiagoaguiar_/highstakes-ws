/* 
 * Copyright 2016 Tiago Aguiar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 * 
 * UserAccountLoader.java criado em Dec 17, 2016
 *
 */
package br.com.hsacademy.loader;

import java.sql.ResultSet;

import br.com.hsacademy.model.UserAccount;
import br.com.taguiar.ResultSetLoader;
import br.com.taguiar.ResultSetWrapper;

/**
 * @author suporte@moonjava.com.br (Tiago Aguiar)
 */
public class UserAccountLoader implements ResultSetLoader<UserAccount> {

  @Override
  public UserAccount get(ResultSet resultSet) {
    ResultSetWrapper rs = new ResultSetWrapper(resultSet, "user_account");
    return new Builder(rs).build();
  }
  
  private class Builder implements UserAccount.Builder {

    private final ResultSetWrapper rs;

    public Builder(ResultSetWrapper rs) {
      this.rs = rs;
    }

    @Override
    public UserAccount build() {
      UserAccount user = new UserAccount(this);
      user.setId(rs.getLong("id"));
      return user;
    }

    @Override
    public String getName() {
      return rs.getString("name");
    }

    @Override
    public String getLastName() {
      return rs.getString("last_name");
    }

    @Override
    public String getEmail() {
      return rs.getString("email");
    }

    @Override
    public String getPassword() {
      return null;
    }

  }

}