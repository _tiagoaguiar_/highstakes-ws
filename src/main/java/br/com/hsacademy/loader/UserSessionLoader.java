/* 
 * Copyright 2016 Tiago Aguiar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 * 
 * UserSessionLoader.java criado em Dec 17, 2016
 *
 */
package br.com.hsacademy.loader;

import java.sql.ResultSet;

import br.com.hsacademy.model.UserAccount;
import br.com.hsacademy.model.UserSession;
import br.com.taguiar.ResultSetLoader;
import br.com.taguiar.ResultSetWrapper;

/**
 * @author suporte@moonjava.com.br (Tiago Aguiar)
 */
public class UserSessionLoader implements ResultSetLoader<UserSession> {

  @Override
  public UserSession get(ResultSet resultSet) {
    ResultSetWrapper rs = new ResultSetWrapper(resultSet, "user_session");
    return new Builder(rs).build();
  }
  
  private class Builder implements UserSession.Builder {

    private final ResultSetWrapper rs;

    public Builder(ResultSetWrapper rs) {
      this.rs = rs;
    }

    @Override
    public UserSession build() {
      return new UserSession(this);
    }

    @Override
    public UserAccount getUserAccount() {
      return new UserAccountLoader().get(rs.getResultSet());
    }

    @Override
    public String getToken() {
      return rs.getString("token");
    }
    
  }
  
}