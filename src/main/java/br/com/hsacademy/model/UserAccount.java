package br.com.hsacademy.model;

import java.time.LocalDateTime;

public class UserAccount {

    private long id;
    private final String name;
    private final String lastName;
    private final String email;
    private final String password;
    private LocalDateTime createdDate;

    public interface Builder extends br.com.taguiar.code.lang.Builder<UserAccount> {

        String getName();

        String getLastName();

        String getEmail();

        String getPassword();
        
    }

    public UserAccount(Builder builder) {
        name = builder.getName();
        lastName = builder.getLastName();
        email = builder.getEmail();
        password = builder.getPassword();
    }
    
    public long getId() {
      return id;
    }
    
    public void setId(long id) {
      this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
    
    public void setCreatedDate(LocalDateTime createdDate) {
      this.createdDate = createdDate;
    }
    
    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

}