/* 
 * Copyright 2016 Tiago Aguiar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 * 
 * UserAccountGuice.java criado em Dec 17, 2016
 *
 */
package br.com.hsacademy.model;

import com.google.inject.Inject;
import com.google.inject.Provider;

import br.com.hsacademy.loader.UserAccountLoader;
import br.com.taguiar.NativeSql;

/**
 * @author suporte@moonjava.com.br (Tiago Aguiar)
 */
public class UserAccountGuice {

  private final Provider<NativeSql> sqlProvider;

  @Inject
  public UserAccountGuice(Provider<NativeSql> sqlProvider) {
    this.sqlProvider = sqlProvider;
  }
  
  public Long insert(UserAccount userAccount) {
    try {
      return sqlProvider.get()
          
          .insert("user_account")
          .add("name", userAccount.getName())
          .add("last_name", userAccount.getLastName())
          .add("email", userAccount.getEmail())
          .add("password", userAccount.getPassword())
          
          .commit();
    } catch (Exception e) {
      if (e.getMessage().contains("Duplicate entry")) {
        return -1l;
      }
    }
    return null;
  }
  
  public UserAccount getByEmail(String email) {
    return newSelect()
        .where("email").equalTo(email)
        .single();
  }
  
  public UserAccount authenticate(String email, String password) {
    return newSelect()
        .where("email").equalTo(email)
        .and("password").equalTo(password)
        .single();
  }
  
  private NativeSql newSelect() {
    return sqlProvider.get()
        .add("SELECT * FROM user_account")
        .andLoadWith(new UserAccountLoader());
  }
  
}