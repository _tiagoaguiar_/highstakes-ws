/* 
 * Copyright 2016 Tiago Aguiar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 * 
 * UserSession.java criado em Dec 17, 2016
 *
 */
package br.com.hsacademy.model;

/**
 * @author suporte@moonjava.com.br (Tiago Aguiar)
 */
public class UserSession {

  private final UserAccount userAccount;
  private final String token;
  
  public interface Builder extends br.com.taguiar.code.lang.Builder<UserSession> {

    UserAccount getUserAccount();

    String getToken();
    
  }

  public UserSession(Builder builder) {
    this.userAccount = builder.getUserAccount();
    this.token = builder.getToken();
  }

  public UserAccount getUserAccount() {
    return userAccount;
  }

  public String getToken() {
    return token;
  }

}