/* 
 * Copyright 2016 Tiago Aguiar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 * 
 * UserSessionGuice.java criado em Dec 17, 2016
 *
 */
package br.com.hsacademy.model;

import com.google.inject.Inject;
import com.google.inject.Provider;

import br.com.hsacademy.loader.UserSessionLoader;
import br.com.hsacademy.util.PasswordGenerator;
import br.com.taguiar.NativeSql;

/**
 * @author suporte@moonjava.com.br (Tiago Aguiar)
 */
public class UserSessionGuice {

  private final Provider<NativeSql> sqlProvider;

  @Inject
  public UserSessionGuice(Provider<NativeSql> sqlProvider) {
    this.sqlProvider = sqlProvider;
  }

  public Long insert(long userAccountId) {
    return sqlProvider.get()

        .insert("user_session")
        .add("user_account_id", userAccountId)
        .add("token", PasswordGenerator.generate(30))

        .commit();
  }
  
  public Long invalidate(String token) {
    return sqlProvider.get()
        
        .delete("user_session")
        .where("token").equalTo(token)
        
        .commit();
  }

  public UserSession getByUserId(long userId) {
    return newSelect()
        .where("user_account_id").equalTo(userId)
        .single();
  }
  
  public UserSession getUserSession(String token) {
    return newSelect()
        .where("token").equalTo(token)
        .single();
  }
  

  private NativeSql newSelect() {
    return sqlProvider.get()
        .add("SELECT * FROM user_session")
        .add("INNER JOIN user_account")
        .add("ON user_session.user_account_id = user_account.id")
        .andLoadWith(new UserSessionLoader());
  }

}