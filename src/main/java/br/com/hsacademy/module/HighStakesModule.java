/* 
 * Copyright 2016 Tiago Aguiar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 * 
 * HighStakesModule.java criado em Dec 17, 2016
 *
 */
package br.com.hsacademy.module;

import com.google.sitebricks.SitebricksModule;

import br.com.hsacademy.page.IndexPage;
import br.com.hsacademy.service.UserAccountService;
import br.com.hsacademy.service.UserSessionService;

/**
 * @author suporte@moonjava.com.br (Tiago Aguiar)
 */
public class HighStakesModule extends SitebricksModule {
  
  @Override
  protected void configureSitebricks() {
    at("/").show(IndexPage.class);
    
    at("/rest/userAccount").serve(UserAccountService.class);
    at("/rest/userSession").serve(UserSessionService.class);
  }

}