/* 
 * Copyright 2016 Tiago Aguiar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 * 
 * UserAccountService.java criado em Dec 17, 2016
 *
 */
package br.com.hsacademy.service;

import com.google.inject.Inject;
import com.google.sitebricks.client.transport.Json;
import com.google.sitebricks.headless.Reply;
import com.google.sitebricks.headless.Request;
import com.google.sitebricks.http.Delete;
import com.google.sitebricks.http.Get;
import com.google.sitebricks.http.Post;

import br.com.hsacademy.model.UserAccount;
import br.com.hsacademy.model.UserAccountGuice;
import br.com.hsacademy.model.UserSession;
import br.com.hsacademy.model.UserSessionGuice;
import br.com.taguiar.sitebricks.http.RequestWrapper;

/**
 * @author suporte@moonjava.com.br (Tiago Aguiar)
 */
public class UserSessionService {

  private final UserAccountGuice userAccountGuice;
  private final UserSessionGuice sessionGuice;

  @Inject
  public UserSessionService(UserAccountGuice userAcccountGuice, UserSessionGuice sessionGuice) {
    this.userAccountGuice = userAcccountGuice;
    this.sessionGuice = sessionGuice;
  }

  @Get
  public Reply<?> get(Request request) {
    RequestWrapper req = new RequestWrapper(request);
    String token = req.getString("token");
    
    UserSession session = sessionGuice.getUserSession(token);
    if (session == null) {
      return Reply.saying().unauthorized();
    }
    UserAccount userAccount = session.getUserAccount();
    return Reply.with(userAccount).as(Json.class).ok();
  }
  
  @Delete
  public Reply<?> invalidate(Request request) {
    RequestWrapper req = new RequestWrapper(request);
    String token = req.getString("token");
    
    long row = sessionGuice.invalidate(token);

    return row > 0 ? Reply.saying().ok() : Reply.saying().error();
  }
  
  @Post
  public Reply<?> post(Request request) {
    RequestWrapper req = new RequestWrapper(request);
    String email = req.getString("email");
    String password = req.getString("password");

    UserAccount user = userAccountGuice.authenticate(email, password);
    if (user == null) {
      return Reply.saying().unauthorized();
    }
    
    sessionGuice.insert(user.getId());
    UserSession session = sessionGuice.getByUserId(user.getId());

    return Reply.with(session).as(Json.class).ok();
  }

}