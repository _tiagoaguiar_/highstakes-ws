/* 
 * Copyright 2016 Tiago Aguiar.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License. 
 * 
 * PasswordGenerator.java criado em Dec 17, 2016
 *
 */
package br.com.hsacademy.util;

import java.security.SecureRandom;
import java.util.Random;

/**
 * @author suporte@moonjava.com.br (Tiago Aguiar)
 */
public class PasswordGenerator {

  private final static String ALPHABET = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

  private static Random wheel;

  static {
    try {
      wheel = SecureRandom.getInstance("SHA1PRNG", "SUN");
    } catch (Exception e) {
      wheel = new Random();
    }
  }

  /**
   * Generate a randon password.
   */
  public static String generate(int length) {
    StringBuilder sb = new StringBuilder(length);

    for (int i = 0; i < length; i++) {
      int digit = wheel.nextInt(36);
      char letter = ALPHABET.charAt(digit);
      sb.append(letter);
    }

    return sb.toString();
  }

}