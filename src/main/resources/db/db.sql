CREATE USER 'hsacademy'@'localhost' IDENTIFIED BY 'hsacademy';
GRANT ALL PRIVILEGES ON * . * TO 'hsacademy'@'localhost';
FLUSH PRIVILEGES;

create database if not exists hsacademy;
use hsacademy;