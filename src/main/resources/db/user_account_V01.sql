drop table if exists user_account;

create table user_account (
	id bigint not null auto_increment,
	name varchar(128) not null,
	last_name varchar(128) not null,
	email varchar(255) not null,
	password text not null,
	disabled tinyint(1) not null default 0,
	created_date timestamp not null default current_timestamp(),
	primary key(id),
	unique key(email)
) engine=InnoDB;