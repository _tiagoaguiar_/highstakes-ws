drop table if exists user_session;

create table user_session (
	id bigint not null auto_increment,
	user_account_id bigint not null,
	token text not null,
	primary key(id)
) engine=InnoDB;